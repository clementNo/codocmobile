import { useNavigation } from "@react-navigation/core";
import { Text, View } from "react-native";

export default function HomeScreen() {
  return (
    <View>
        <Text >"Explorer les données de l|'entrepôt"</Text>
       <Text>"Profondeur de la donnée et exhaustivité"</Text>
        <Text>"Votre entrepôt de données de santé (EDS) grandit avec le temps. Ce diagramme vous permet de visualiser le chargement de votre EDS et la profondeur de données correspondante. La barre d'exhaustivité permet de visualiser l\'année à partir de laquelle vous devriez retrouver plus de 99,5% de vos patients."</Text>
    </View>
  );
}
