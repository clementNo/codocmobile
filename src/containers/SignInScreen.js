import { useNavigation } from "@react-navigation/core";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Button, Text, TextInput, View, TouchableOpacity, Image } from "react-native";

export default function SignInScreen({ setToken }) {
  const navigation = useNavigation();
  return (
      <KeyboardAwareScrollView>
    <View>
      <View>
        <Image
          source={require("../../assets/img/LoginGeneral.png")}
        />
        <Text>Name: </Text>
        <TextInput placeholder="Username" />
        <Text>Password: </Text>
        <TextInput placeholder="Password" secureTextEntry={true} />
        <Button
          title="Sign in"
          onPress={async () => {
            const userToken = "secret-token";
            setToken(userToken);
          }}
        />
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("SignUp");
          }}
        >
          <Text>Create an account</Text>
        </TouchableOpacity>
      </View>
    </View>
      </KeyboardAwareScrollView>
  );
}
