import { RiMenuFill } from "react-icons/ri";
//import SideMenu from "./SideMenu";
import { StyleSheet } from "react-native";
import {Button, Image} from "react-native";
import {useNavigation} from "@react-navigation/core";

const Header = () => {
    const navigation = useNavigation();
    return  <div >
                <Image
                    style={styles.logo}
                    source={require('../../assets/img/logo/white/Care.png')}
                    resizeMode='contain'
                />,
                <Button
                    title="Go to Profile"
                    onPress={() => {
                        navigation.navigate("Profile", { userId: 123 });
                    }}
                />
            </div>;
}

const styles = StyleSheet.create({
    logo: {
        marginLeft: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
        height: 45,
    },
});

export default Header;